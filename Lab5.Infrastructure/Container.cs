﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PK.Container;

namespace Lab5.Infrastructure
{
    public class Container : IContainer
    {
        IDictionary<Type, Type> slowniktypy;
        IDictionary<Type, object> slownikobiekty;
        IDictionary<Type, Delegate> slownikdelegaty;
        public Container()
        {
            slowniktypy = new Dictionary<Type, Type>();
            slownikobiekty = new Dictionary<Type, object>();
            slownikdelegaty = new Dictionary<Type, Delegate>();
        }
        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (Type i in assembly.GetExportedTypes())
            {
                Register(i);
            }
        }

        public void Register(Type type)
        {
            foreach (var i in type.GetInterfaces())
            {
                if (!slowniktypy.ContainsKey(i))
                {
                    slowniktypy.Add(i, type);
                }
                else
                {
                    slowniktypy[i] = type;
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            if (!(typeof(T).IsInterface))
            {
                foreach (var i in typeof(T).GetInterfaces())
                {
                    if (!slownikobiekty.ContainsKey(i))
                    {
                        slownikobiekty.Add(i, impl);
                    }
                    else
                    {
                        slownikobiekty[i] = impl;
                    }
                }
            }
            else
            {

                if (!slownikobiekty.ContainsKey(typeof(T)))
                {
                    slownikobiekty.Add(typeof(T), impl);
                }
                else
                {
                    slownikobiekty[typeof(T)] = impl;
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            if (!(typeof(T).IsInterface))
            {
                foreach (var i in typeof(T).GetInterfaces())
                {
                    if (!slownikdelegaty.ContainsKey(i))
                    {
                        slownikdelegaty.Add(i, provider);
                    }
                    else
                    {
                        slownikdelegaty[i] = provider;
                    }
                }
            }
            else
            {

                if (!slownikdelegaty.ContainsKey(typeof(T)))
                {
                    slownikdelegaty.Add(typeof(T), provider);
                }
                else
                {
                    slownikdelegaty[typeof(T)] = provider;
                }
            }
        }

        public T Resolve<T>() where T : class
        {
            return Resolve(typeof(T)) as T;
        }

        public object Resolve(Type type)
        {
            if (!type.IsInterface)
            {
                throw new ArgumentException("Typ nie jest interfejsem");

            }
            else
            {
                if (!slownikobiekty.ContainsKey(type))
                {
                    if (!slownikdelegaty.ContainsKey(type))
                    {
                        if (!slowniktypy.ContainsKey(type))
                        {
                            return null;
                        }
                        else
                        {

                            List<object> parametry = new List<object>();
                            int ilosc = 0;
                            foreach (var konstruktory in slowniktypy[type].GetConstructors())
                            {
                                var parametry2 = konstruktory.GetParameters();
                                if (parametry2.Count() > ilosc)
                                {
                                    ilosc=0;
                                    foreach ( var k in parametry2)
                                    {
                                        if (k.ParameterType.IsInterface)
                                        {
                                            ilosc = ilosc + 1;
                                        }
                                    }
                                    parametry.Clear();
                                    object wynik = null;
                                    foreach (var k in parametry2)
                                    {
                                        if (k.ParameterType.IsInterface)
                                        {
                                            wynik = Resolve(k.ParameterType);
                                        }
                                        if (wynik == null)
                                        {
                                            throw new UnresolvedDependenciesException();
                                        }
                                        parametry.Add(wynik);
                                    }
                                }
                            }
                            var wynik2 = Activator.CreateInstance(slowniktypy[type], parametry.ToArray());
                            return wynik2;
                        }
                    }
                    else
                    {
                        return slownikdelegaty[type].DynamicInvoke();

                    }

                }
                else
                {
                    return slownikobiekty[type];

                }
            }
        }
    }
}

