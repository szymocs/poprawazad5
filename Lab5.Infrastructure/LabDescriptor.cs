﻿using System;
using System.Reflection;
using Lab5.Infrastructure;


namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Lab5.Główny.Kontrakt.IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Lab5.Główny.Implemetacja.Glowny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(Lab5.Wyświetlacz.Kontrakt.IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Lab5.Wyświetlacz.Implementacja.Wyswietlacz));

        #endregion
    }
}
