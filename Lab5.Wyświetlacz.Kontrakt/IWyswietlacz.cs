﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab5.Wyświetlacz.Kontrakt
{
  public interface IWyswietlacz
    {
        void Myjnia();
        void Myje();
        void Czysci();
        void Suszy();
        void Koniec();

    }
}
